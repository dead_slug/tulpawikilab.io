---
title: Домашняя страница
description: TulpaWiki - управляемый сообществом и (возможно) самый большой источник информации о тульпах.
aliases:
  - /index
  - /ru/index
---
**TulpaWiki** – это ресурс, посвящённый феномену тульпы, включающий в себя:
* **Архив тематических материалов**, обеспечивающий компактное и структурированное хранение гайдов, методологических заметок, художественных произведений и дампов тематических тредов;
* **Штаб оригинальных исследований**, вовлечённый в анализ гайдов, поиск закономерностей и написание улучшенных пособий, в изучение истории тульповодства;
* **Кружок наблюдателей и переводчиков**, снабжающий русскоязычное тульповодское сообщество свежей информацией с англоязычных форумов, блогов и имиджборд;
